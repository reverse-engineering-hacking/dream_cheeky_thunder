# Dream Cheeky Thunder RE Writeup

##### Author: Daniel Lecklider
##### Date: 2022-02-04

![Dream Cheeky Thunder](https://cdn-o.fishpond.com.au/0036/998/026/54593961/original.jpeg)

## Table of Contents
1. [Challenge Description](#challenge-description)
2. [Tools Used](#tools-used)
3. [Introduction](#introduction)
4. [Driver Application Analysis](#driver-application-analysis)
5. [Device Packet Analysis](#device-packet-analysis)
6. [Protocol Re-Implementation](#protocol-re-implementation)
7. [Follow On](#follow-on)
8. [Summary](#summary)

## Challenge Description

The goal of this challenge was to determine the protocol and methodology in use by the driver software to control the nerf turret and then subsequently re-implement the capabilities using none of the original manufacturer software to be able to successfully aim and hit the opposing team participating in the hackaton.

The participants were able to connect the hardware device to their personal computer and no restrictions were held on being able to run the manufacturers driving software or control the turret during analysis. The only restriction was that no existing source code could be looked up online for the hardware in question and that none of the manufacturer's software binaries could be used in order to be considered a winning team. 

## Tools Used

- [Virtual Box](https://www.virtualbox.org/)
- [Ghidra](https://ghidra-sre.org/)
- [Dependency Walker](https://www.dependencywalker.com/)
- [dnSpy](https://github.com/dnSpy/dnSpy)
- [Python](https://www.python.org/)
    - [PyQt5](https://pypi.org/project/PyQt5/)
    - [hid](https://pypi.org/project/hid/)
## Introduction:

Beginning the challenge I had no knowledge of how the device worked under the hood, and additionally I have no professional experience with the USB protocol. The only thing that myself and coworker, Brandon Rothel, knew was that the driver application required .NET 2.0 (yes... this is a very old hardware/software combo). 

With any software/hardware system that you are not fully confident in, it is usually a good idea to run things within a closed environment (such as a virtual machine). With that in mind all of the work done for this challenge was performed within a VM so as to prevent running an arbitrary executable on my personal computer.

## Driver Application Analysis:

The first thing I did was run Ghidra's analysis process on the program, but didn't spend much time on this as I did not find anything that immediately jumped out to me, and also remembered that the software required some dependency on the .NET framework for it to run. With this knowledge in mind and some prior knowledge that .NET binaries often can have near full source code reproduction from a quality C# decompiler. 

I then ran dependency walker on the top level driver application to see what we were dealing with. The two notable things here were that the driver exe depended on `MSCOREE.dll` [which means that is is a managed binary](https://reverseengineering.stackexchange.com/a/1619), and additionally it linked against what appeared to be a custom written USB library: `USBLib.DLL`. Running dependency walker on this USB binary as well showed it too was a C# binary. 

With the new knowledge that the application was written fully in C# I went searching online for a C# decompiler and stumbled upon [dnSpy](https://github.com/dnSpy/dnSpy). With this I was able to successfully analyze and decompile both the driver exe and the USB library. Additionally as the dnSpy application appeared to have debugging capabilities and there were no restrictions in the hackathon to the execution of the driver software I decided to use dynamic analysis for the rest of the challenge. 

dnSpy was able to recreate the source code at an astounding level with essentially no level of visible obfuscation. 

![dnSpy View of THUNDER.exe](img/dnSpy_source.png)

 Looking through the functions at a top level I found one that caught my eye `SendUSBData` and placed a breakpoint there. Upon execution of the driver program it immediately hit this breakpoint and I traced the call-stack to determine the initialization sequence that was occurring. Allowing the application to continue after the initial breakpoint was hit I then began to catalogue all of the packets data structures for each primary physical action (move left, move up, shoot, etc). 
 
 - The manufacturer's driver application appears to send data in this order to initialize the device, however at a later time it is discovered that this is not required for the device to start accepting commands.
- There appears to be two different command profiles. This is entirely dependent on the version of the USB object within the greater class. This is determined by which physical version of the device you have. All participants at the hackathon in which this device was originally reverse engineered had the same device type however.
    - This device in question has ProductId = 0x00001010 which is 4112 in decimal.
    - Because of this device id the device is in particular is set into version profile 1
    - This command profile difference simply appears to change the packet payloads. No additional functionality appears to be exposed from one command profile to another.
- Status messages appear to flow back from the device determining what state it is currently in. Device states that are returned are as follows:
    - IDLE
    - Down Limit
    - Up Limit
    - Left Limit
    - Right Limit
    - Fired
- Apparently this device is registered as an HID device. This being said my particular device had the following device details:
    - VID: 0x2123 - 8483
    - PID: 0x1010 - 4112

## Device Packet Analysis:

During analysis of the packet structure I also saw that after each data payload being sent to the device the driver program would check for a status returned. This status contained information for if the device had reached slew limits in one/both of its axes, or if it was in the process of firing. The finally thing worth noting is that for a single slewing command ushered to the device, it will continue with that command until a `STOP` command has been issued.

Once this data collection was complete and I knew the raw data payloads I needed to learn a bit more about the USB protocol and how things were being transmitted. Looking deeper into the call-stack I saw that the USB library was using the HID (Human Interface Device) protocol (which admittedly seems strange to me for this device type, but I am no expert on USB protocol usage/standards). [This](http://www.usblyzer.com/usb-human-interface-device-hid-class-decoder.htm#:~:text=HID%20Report%20Descriptor%20defines%20the,report%20of%20the%20same%20type) page gave me some good insight into the HID protocol and what they call "reports" that get sent back and forth between the computer and the HID device. 

- Data appears to be variable packet length based on packet type. However most packets appear to be 9 bytes or 10 bytes long. 
- Discovered packets data payloads are as follows:

    1. "Administrative" packets
        1. Turn LED on
            - 0x00
            - 0x03
            - 0x01
            - 0x00
            - 0x00
            - 0x00
            - 0x00
            - 0x00
            - 0x00

        2. Turn LED off
            - 0x00
            - 0x03
            - 0x00
            - 0x00
            - 0x00
            - 0x00
            - 0x00
            - 0x00
            - 0x00

    2. Data Packets
        1. Fire:
            - Data:
            - 0x00
            - 0x02
            - 0x10
            - 0x00
            - 0x00
            - 0x00
            - 0x00
            - 0x00
            - 0x00
            - 0x00
        2. Up:
            - 0x00
            - 0x02
            - 0x02
            - 0x00
            - 0x00
            - 0x00
            - 0x00
            - 0x00
            - 0x00
            - 0x00
        3. Left:
            - 0x00
            - 0x02
            - 0x04
            - 0x00
            - 0x00
            - 0x00
            - 0x00
            - 0x00
            - 0x00
            - 0x00
        4. Right:
            - 0x00
            - 0x02
            - 0x08
            - 0x00
            - 0x00
            - 0x00
            - 0x00
            - 0x00
            - 0x00
            - 0x00
        5. Down:
            - 0x00
            - 0x02
            - 0x01
            - 0x00
            - 0x00
            - 0x00
            - 0x00
            - 0x00
            - 0x00
            - 0x00
        6. Stop:
            - 0x00
            - 0x02
            - 0x20
            - 0x00
            - 0x00
            - 0x00
            - 0x00
            - 0x00
            - 0x00
            - 0x00

## Protocol Re-implementation

Once I had these two pieces under my belt (driving application methodology, and packet data/protocol) I was able to start writing the re-implementation of the software. As this challenge was time constrained I decided to write things in Python to keep the development and iteration time to a speedy clip. I found [this](https://github.com/apmorton/pyhidapi) Python HID library which I decided to utilize. Additionally I decided to write a gui so when I got the data packet format down I could quickly slew the USB turret towards my "opponent" and fire away. For this GUI I decided to use [PyQt5](https://pypi.org/project/PyQt5/). 

After this it took some fiddling to get the Python source code written up and fiddling with the Python type system and the HID library to make sure the data was in an acceptable format, but once this was complete the device started responding brilliantly to my commands.

## Follow On
While unrelated to reverse engineering the group discussed some logical follow on for this small project. Now that we have a fully functional re-implementation of the protocol being used for this device, the group discussed mounting a small embedded camera to the device and integrating/developing a small machine learning module to perform facial/target recognition for the device. This would allow the device to potentially be fully self sufficient (no driving computer connection required) and would allow for hands on experience training machine learning models and writing embedded code for the integrated driver hardware (likely a small microcontroller or raspberry pi).
## Summary

After I got the re-implementation of the protocol, user facing application developed, and had claimed victory (successfully firing and hitting a nerf dart from the turret at the opposing team), I had a really great post-mortem with co-worker Brandon Rothel as he approached the challenge from a completely opposite perspective and used Wireshark to perform his RE and did no static or dynamic binary analysis. If Brandon posts his write-up, I will link it here as well. 

There was additionally some discussion about the protocol in use by the hardware as the packet structure functionally can allow for opposing movement commands to be issued simultaneously (left/right or up/down). With this knowledge there was some speculation as to if there was a cyber-physical exploit potentially capable on the system. Depending on if there was one or two motors per axis, or the wiring of the device and underlying hardware control there was speculation that if the manufacturers did not implement things securely/robustly that the system may be able to be rendered disabled or broken by issuing one of these commands. As the devices are not easy to find and there was logical follow on discussed above, this theory remained a theory and was not tested in any way shape or form. 

All in all the challenge took a little under 3 hours (2 hours 55 minutes), and was a great learning exercise proving that there are multiple routes to take when profiling down the functionality of a physical system and learning about all of the protocols involved.