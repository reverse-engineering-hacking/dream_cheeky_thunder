import sys
import time

from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QHBoxLayout, QGroupBox, QDialog, QVBoxLayout, QGridLayout
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import pyqtSlot

from cheeky_hack import *

with hid.Device(vid, pid) as h:
    def up_fnc():
        h.write(CMD_UP)
        time.sleep(TIME_GENERIC_SLEEP)
        h.write(CMD_STOP)

    def down_fnc():
        h.write(CMD_DOWN)
        time.sleep(TIME_GENERIC_SLEEP)
        h.write(CMD_STOP)

    def fire_fnc():
        h.write(CMD_FIRE)

    def left_fnc():
        h.write(CMD_LEFT)
        time.sleep(TIME_GENERIC_SLEEP)
        h.write(CMD_STOP)

    def right_fnc():
        h.write(CMD_RIGHT)
        time.sleep(TIME_GENERIC_SLEEP)
        h.write(CMD_STOP)         

    class App(QDialog):

        def __init__(self):
            super().__init__()
            self.title = 'Cheeky HACKED'
            self.left = 400
            self.top = 400
            self.width = 320
            self.height = 100
            self.initUI()
            
        def initUI(self):
            self.setWindowTitle(self.title)
            self.setGeometry(self.left, self.top, self.width, self.height)
            
            self.createGridLayout()
            
            windowLayout = QVBoxLayout()
            windowLayout.addWidget(self.horizontalGroupBox)
            self.setLayout(windowLayout)
            
            h.write(CMD_LED_ON)

            self.show()
        
        def closeEvent(self, event):
            h.write(CMD_LED_OFF)

        def createGridLayout(self):
            self.horizontalGroupBox = QGroupBox("Controls")
            layout = QGridLayout()
            layout.setColumnStretch(1, 4)
            layout.setColumnStretch(2, 4)
            
            layout.addWidget(QPushButton('Up'),0,1)
            layout.addWidget(QPushButton('Left'),1,0)
            layout.addWidget(QPushButton('Fire'),1,1)
            layout.addWidget(QPushButton('Right'),1,2)
            layout.addWidget(QPushButton('Down'),2,1)

            # Hook in buttons
            widget1 = layout.itemAtPosition(0,1).widget()
            widget1.clicked.connect(up_fnc)

            widget2 = layout.itemAtPosition(1,0).widget()
            widget2.clicked.connect(left_fnc)

            widget3 = layout.itemAtPosition(1,1).widget()
            widget3.clicked.connect(fire_fnc)

            widget4 = layout.itemAtPosition(1,2).widget()
            widget4.clicked.connect(right_fnc)

            widget5 = layout.itemAtPosition(2,1).widget()
            widget5.clicked.connect(down_fnc)                                
            
            self.horizontalGroupBox.setLayout(layout)

    if __name__ == '__main__':
        app = QApplication(sys.argv)
        ex = App()
        sys.exit(app.exec_())
