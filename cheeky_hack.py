import os
import pathlib

# The hid python library relys on some specific wrapper DLLs. In order to prevent putting them in the system or user path we augment the path here. 
HID_DLL_PATH = pathlib.Path(__file__).parent.resolve()
os.add_dll_directory(HID_DLL_PATH)

import hid

vid = 8483
pid = 4112

TIME_GENERIC_SLEEP=0.1

CMD_LED_ON=bytes(bytearray([0, 3, 1, 0, 0, 0, 0, 0, 0]))
CMD_LED_OFF=bytes(bytearray([0, 3, 0, 0, 0, 0, 0, 0, 0]))
CMD_DOWN=bytes(bytearray([0, 2, 1, 0, 0, 0, 0, 0, 0]))
CMD_UP=bytes(bytearray([0, 2, 2, 0, 0, 0, 0, 0, 0]))

CMD_LEFT=bytes(bytearray([0, 0x02, 0x04, 0, 0, 0, 0, 0, 0]))
CMD_RIGHT=bytes(bytearray([0, 0x02, 0x08, 0, 0, 0, 0, 0, 0]))

CMD_FIRE=bytes(bytearray([0, 2, 0x10, 0, 0, 0, 0, 0, 0]))
CMD_STOP=bytes(bytearray([0, 2, 0x20, 0, 0, 0, 0, 0, 0]))